
all: compile

compile: pppp13 armus-x10/target/armusc-x10.jar
	(cd pppp13; make checked)

run: logs/wfg logs/sg logs/origin logs/auto logs/teg benchmarktk compile
	./benchmarktk/run-benchmark setup.yaml run.yaml

run-gs: logs/gs-auto logs/gs-wfg logs/gs-sg benchmarktk compile
	./benchmarktk/run-benchmark setup.yaml run-gs.yaml

collect: data/auto data/wfg data/sg data/origin stats-tk
	./benchmarktk/collect-data setup.yaml stats.yaml auto.yaml
	./benchmarktk/collect-data setup.yaml stats.yaml sg.yaml
	./benchmarktk/collect-data setup.yaml stats.yaml wfg.yaml
	./benchmarktk/generate-stats stats-gs.yaml

recompile:
	(cd armus-x10; ant)
	(cd pppp13; make clean; make checked)

data/auto:
	mkdir -p $@

data/wfg:
	mkdir -p $@

data/sg:
	mkdir -p $@

data/origin:
	mkdir -p $@

logs/auto:
	mkdir -p $@

logs/wfg:
	mkdir -p $@

logs/sg:
	mkdir -p $@

logs/teg:
	mkdir -p $@

logs/origin:
	mkdir -p $@

logs/gs-auto:
	mkdir -p $@

logs/gs-wfg:
	mkdir -p $@

logs/gs-sg:
	mkdir -p $@

logs/sg-teg:
	mkdir -p $@

armus-x10/target/armusc-x10.jar: armus-x10
	(cd armus-x10; ant)

sync: armus-x10 pppp13 benchmarktk stats-tk
	(cd armus-x10; git pull)
	(cd pppp13; git pull)
	(cd benchmarktk; git pull)
	(cd stats-tk; git pull)

armus-x10:
	git clone https://bitbucket.org/cogumbreiro/armus-x10/

pppp13:
	git clone https://bitbucket.org/cogumbreiro/pppp13/

benchmarktk:
	git clone https://bitbucket.org/cogumbreiro/benchmarktk/

stats-tk:
	git clone https://bitbucket.org/cogumbreiro/stats-tk/
